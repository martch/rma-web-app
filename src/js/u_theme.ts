export function u_toggleTheme(el: HTMLElement) {
	const html = document.querySelector("html")
	if (!html) {
		return console.error("Could not get html tag. Aborting operation.")
	}
	const htmlClasses = html.classList
	if (!htmlClasses) {
		return console.error("Could not get html tag's class list. Aborting operation.")
	}

	if (htmlClasses.contains("theme-dark")) {
		el.innerHTML = "☼ → ☾"
		htmlClasses.remove("theme-dark")
		htmlClasses.add("theme-light")
		localStorage.setItem("theme", "theme-light")
	} else {
		el.innerHTML = "☾ → ☼"
		htmlClasses.remove("theme-light")
		htmlClasses.add("theme-dark")
		localStorage.setItem("theme", "theme-dark")
	}
}
