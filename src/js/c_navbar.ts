import { c_dropdown } from "./c_dropdown";
import { UI_TXT, capitalize, lang } from "./u_language";
import { SEARCH_BOX } from "./index";
import { t_a, t_button, t_div, t_nav } from "./u_html-tags";
import { u_toggleTheme } from "./u_theme";
import { logout } from "./sdk/pocketbase/auth";

export function c_navbar() {
	const navbar = t_nav({
		class: ["grid", "navbar"],
		id: "navbar",
	}, [
		t_div({ class: ["grid", "navbar__navigation-links"] }, [
			t_a({
				class: ["button", "btn--gray", "btn--normal"],
				id: "main_page",
				href: "/main",
				text: capitalize(UI_TXT.overview[lang], "first"),
				useRouter: true
			}),
			t_a({
				class: ["button", "btn--green", "btn--normal"],
				id: "add_new_case_page",
				href: "/new",
				text: capitalize(UI_TXT.new_case[lang], "first"),
				useRouter: true
			}),
			t_a({
				class: ["button", "btn--gray", "btn--normal"],
				href: "/main",
				text: capitalize(UI_TXT.transport[lang], "first"),
				useRouter: true
			}),
			t_a({
				class: ["button", "btn--gray", "btn--normal"],
				href: "/main",
				text: capitalize(UI_TXT.reports[lang], "first"),
				useRouter: true
			})
		]),
		SEARCH_BOX,
		t_button({
			class: ["navbar__light-toggle", "btn", "btn--gray", "btn--normal"],
			text: (() => {
				if (document.querySelector("html")?.classList.contains("theme-dark")) {
					return "☾ → ☼"
				} else {
					return "☼ → ☾"
				}
			})(),
			onclick: (el: Event) => { u_toggleTheme(el.target as HTMLElement) }
		}),
		c_dropdown(
			capitalize(UI_TXT.options[lang], "first"),
			["navbar__options"],
			[
				t_a({
					class: ["button", "btn--gray", "btn--normal"],
					href: "/settings",
					text: capitalize(UI_TXT.settings[lang], "first"),
					useRouter: true
				}),
				t_button({
					class: ["btn", "btn--red", "btn--normal"],
					id: "logout_button",
					text: capitalize(UI_TXT.logout[lang], "first"),
					onclick: () => {
						try { logout() }
						catch (error) { console.error(error) }
					}
				}),
			]),
	])

	return navbar
}
