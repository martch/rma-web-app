import { get_USER, TAGS } from "./index"
import { UI_TXT, capitalize, lang } from "./u_language"
import { route } from "./u_router"
import { t_a, t_button, t_div, t_fieldset, t_form, t_legend, t_textarea } from "./u_html-tags"
import { LogRecord, Tag, UserRecord } from "./interface"
import { addLogRecord, getActiveTagIds, getAllUserNames, getLogs, setActiveTags } from "./sdk/pocketbase/db"
import { createTag } from "./c_tag"

let activeTagsIds: Array<string> = []
let activeTags: Array<Tag> = []
let logRecords: Array<LogRecord> = []

export async function c_log() {
    const params = new URLSearchParams(window.location.search)
    const caseId = params.get("caseId")
    const productId = params.get("prodId")

    if (!caseId || !productId) {
        console.error("Path is missing a parameter.")
        window.history.replaceState(null, "", "/404")
        route(null, "/404")
        return ""
    }

    const log = t_div({ class: ["grid", "log"] }, [
        c_topBar(caseId, productId),
        newCommentSection(productId)
    ])

    try { u_updateChangelog(productId, log) }
    catch (error) { console.error(error) }

    return log
}

function c_topBar(caseId: string, productId: string) {
    return t_div({ class: ["grid", "log__top-bar"] }, [
        t_div({ class: "log__top-bar__case-id" }, [
            `${capitalize(UI_TXT.case_id[lang], "first")}: ${caseId}`
        ]),
        t_div({ class: "log__top-bar__product-index" }, [
            `${capitalize(UI_TXT.product_index[lang], "first")}: ${productId}`
        ]),
        t_a({
            text: caseId,
            href: `/case?id=${caseId}`,
            useRouter: true,
        })
    ])
}

async function c_timeline() {
    document.querySelector("#timeline")?.remove()

    const timeline = t_div({ class: "log__timeline", id: "timeline" })

    for (let i = 0; i < logRecords.length; i++) {
        switch (logRecords[i].type) {
            case "tag_add":
                try { timeline.append(await c_entry("tag_add", logRecords[i])) }
                catch (error) { console.error(error) }
                break
            case "tag_remove":
                try { timeline.append(await c_entry("tag_remove", logRecords[i])) }
                catch (error) { console.error(error) }
                break
            case "comment_add":
                try { timeline.append(await c_entry("comment_add", logRecords[i])) }
                catch (error) { console.error(error) }
                break
        }
    }

    return timeline
}

async function c_entry(type: "comment_add" | "comment_edit" | "tag_add" | "tag_remove", data: LogRecord) {
    let timeCreated = data.created
    if (!timeCreated) { throw new Error(`timeCreated is ${timeCreated}`) }

    const timeSinceAdded = new Date().getTime() - new Date(timeCreated).getTime()
    const second = 1000
    const minute = 60 * second
    const hour = 60 * minute
    const day = 24 * hour

    const rtf = new Intl.RelativeTimeFormat(lang, { style: 'short' })
    const dtf = new Intl.DateTimeFormat('lv', {
        dateStyle: 'short',
        timeStyle: 'short'
    })

    let formattedTimeSinceAdded: string

    if (timeSinceAdded < minute) {
        formattedTimeSinceAdded = rtf.format(Math.floor(-1 * timeSinceAdded / second), "seconds")
    } else if (timeSinceAdded < hour) {
        formattedTimeSinceAdded = rtf.format(Math.floor(-1 * timeSinceAdded / minute), "minutes")
    } else if (timeSinceAdded < day) {
        formattedTimeSinceAdded = rtf.format(Math.floor(-1 * timeSinceAdded / hour), "hours")
    } else {
        formattedTimeSinceAdded = dtf.format(new Date(timeCreated))
    }

    let userNames: Array<UserRecord>
    try { userNames = await getAllUserNames() }
    catch (error) { throw new Error(String(error)) }

    const userId = data.user_id
    if (!userId) { throw new Error(`userId is ${userId}`) }

    const entry = t_div({ id: data.id })

    for (let i = 0; i < userNames.length; i++) {
        if (userNames[i].id !== userId) { continue }

        const username = userNames[i].name

        switch (type) {
            case "tag_add":
                for (let i = 0; i < TAGS.length; i++) {
                    if (TAGS[i].id === data.tag_id) {
                        entry.innerText = `${formattedTimeSinceAdded}:
                            ${username} pievienoja tagu "${TAGS[i].value[lang]}"`
                    }
                }
                break
            case "tag_remove":
                entry.innerText = `${formattedTimeSinceAdded}:
                    ${username} noņēma tagu "${data.tag_id}"`
                break
            case "comment_add":
                entry.append(c_postedComment(formattedTimeSinceAdded, username, data))
                break
            default:
                break
        }
    }

    return entry
}

function c_postedComment(timestamp: string, username: string, data: LogRecord) {
    const comment = t_fieldset({}, [
        t_legend({ class: ["log__log__entry__legend--comment", "grid"] }, [
            t_div({}, [
                `${username} ${UI_TXT.commented[lang]} ${timestamp}`
            ]),
            t_button({ text: "..." })
        ]),
        t_div({}, [
            data.comment ? data.comment : ""
        ])
    ])

    return comment
}

async function c_tags(productId: string) {
    document.querySelector("#tags")?.remove()

    const activeTagsSection = t_div({
        class: "log__tags__list"
    })
    const availableTagsSection = t_div({
        class: ["shadow", "log__tags__select"],
        id: "tags_select"
    })

    availableTagsSection.addEventListener(
        "mouseleave",
        () => { availableTagsSection.remove() })
    TAGS.forEach((tag) => {
        let isTagActive: boolean = false
        for (let i = 0; i < activeTags.length; i++) {
            if (activeTags[i].name === tag.name) {
                isTagActive = true
                return
            }
        }

        if (!isTagActive) {
            const el = createTag(tag.value[lang], tag.color)
            el.addEventListener("click", () => { u_addTag(productId, tag) })
            availableTagsSection.append(el)
        }
    })

    const tagsSection = t_div({
        class: ["grid", "log__tags"],
        id: "tags"
    }, [
        t_button({
            class: "log__tags__add",
            text: capitalize(UI_TXT.add[lang], "first"),
            onclick: () => { tagsSection.append(availableTagsSection) }
        }),
        activeTagsSection
    ])

    if (activeTags.length > 0) {
        for (let i = 0; i < activeTags.length; i++) {
            const activeTag = activeTags[i]
            const tag = createTag(activeTag.value[lang], activeTag.color)
            tag.addEventListener("click", async () => {
                if (confirm("Noņemt tagu?")) await u_removeTag(activeTag)
            })
            activeTagsSection.append(tag)
        }
    }

    async function u_addTag(productId: string, tag: Tag) {
        const user = get_USER()
        if (!user) { return console.error(`user is ${user}`) }
        if (!user.id) { return console.error(`user.id is ${user.id}`) }

        const record: LogRecord = {
            product_id: productId,
            type: "tag_add",
            user_id: user.id,
            tag_id: tag.id
        }

        try { await addLogRecord(record) }
        catch (error) { return console.error(error) }

        activeTagsIds.push(tag.id)

        try { await setActiveTags(productId, activeTagsIds) }
        catch (error) { return console.error(error) }

        console.info("New tag added succesfully.")

        try { u_updateChangelog(productId) }
        catch (error) { console.error(error) }
    }

    async function u_removeTag(tag: Tag) {
        const user = get_USER()
        if (!user) { return console.error(`user is ${user}`) }
        if (!user.id) { return console.error(`user.id is ${user.id}`) }

        const record: LogRecord = {
            product_id: productId,
            type: "tag_remove",
            user_id: user.id,
            tag_id: tag.id
        }

        try { await addLogRecord(record) }
        catch (error) { return console.error(error) }

        activeTagsIds.splice(activeTagsIds.indexOf(tag.id), 1)

        try { await setActiveTags(productId, activeTagsIds) }
        catch (error) { return console.error(error) }

        console.info("New tag removed succesfully.")

        try { u_updateChangelog(productId) }
        catch (error) { console.error(error) }
    }

    return tagsSection
}

function newCommentSection(prodId: string) {
    return t_div({ class: ["log__comment"] }, [
        t_form({ class: ["grid", "log__comment__form"] }, [
            t_textarea({
                class: "log__comment__form__textarea",
                id: "new_comment_content"
            }),
            t_button({
                class: "log__comment__form__add-button",
                text: capitalize(UI_TXT.add[lang], "first"),
                onclick: async () => {
                    const textarea: HTMLTextAreaElement | null =
                        document.querySelector("#new_comment_content")
                    if (!textarea) {
                        return console.error(`textarea is ${textarea}`)
                    }
                    try { await u_addComment(prodId, textarea.value) }
                    catch (error) { console.error(error) }
                    textarea.value = ""
                }
            })
        ])
    ])
}

async function u_addComment(productId: string, commentRaw: string) {
    const comment = commentRaw.trim()
    if (comment === "") {
        return console.warn("Adding empty comments is strictly forbidden.")
    }

    const record: LogRecord = {
        product_id: productId,
        type: "comment_add",
        user_id: get_USER()?.id,
        comment: comment,
    }

    try { await addLogRecord(record) }
    catch (error) { throw new Error(String(error)) }

    console.info("New comment added succesfully.")

    try { u_updateChangelog(productId) }
    catch (error) { console.error(error) }
}

async function u_updateChangelog(productId: string, log?: HTMLDivElement) {
    let parent = log
    if (!parent) {
        parent = document.querySelector(".log") as HTMLDivElement | undefined
        if (!parent) { throw new Error(`parent is ${parent}`) }
    }

    logRecords = []

    try { logRecords = await getLogs(productId) }
    catch (error) { throw new Error(String(error)) }

    activeTagsIds = []

    let resultActiveTags: Array<string>
    try { resultActiveTags = await getActiveTagIds(productId) }
    catch (error) { throw new Error(String(error)) }

    activeTagsIds = resultActiveTags

    activeTags = []
    if (activeTagsIds.length === 0) {
        console.warn("Product has no active tags.")
    } else {
        for (let i = 0; i < activeTagsIds.length; i++) {
            for (let j = 0; j < TAGS.length; j++) {
                if (TAGS[j].id === activeTagsIds[i]) {
                    activeTags.push(TAGS[j])
                }
            }
        }
    }

    let timeline: HTMLDivElement
    try { timeline = await c_timeline() }
    catch (error) { throw new Error(String(error)) }

    parent.append(await c_tags(productId), timeline)
}
