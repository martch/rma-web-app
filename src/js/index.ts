import { c_filters } from "./c_filters"
import { c_navbar } from "./c_navbar"
import { c_root } from "./c_root"
import { c_searchBox } from "./c_search-box"
import { getRequestedSolution, getTags } from "./sdk/pocketbase/db"
import { PB } from "./sdk/pocketbase/init"
import { RequestedSolution, Tag, UserRecord } from "./interface"
import { handleLocation } from "./u_router"

async function initApp() {
    try { await loadApp() }
    catch (error) { console.error(error) }

    PB.authStore.onChange(async () => {
        if (!PB.authStore.record) {
            set_USER(null)
            handleLocation()
            return
        }
        set_USER(PB.authStore.record as UserRecord)
        handleLocation()
    }, true)
}

let USER: UserRecord | null = null
export function set_USER(value: UserRecord | null) { USER = value }
export function get_USER() { return USER }

interface AppData {
    requestedSolutions: RequestedSolution[] | undefined
}

export const APP_DATA: AppData = {
    requestedSolutions: undefined
}

export let SEARCH_BOX: HTMLDivElement;
export let ROOT: HTMLDivElement;
export let FILTERS: HTMLDivElement;
export let NAVBAR: HTMLElement;

export let TAGS: Array<Tag>

export async function loadApp() {
    try { APP_DATA.requestedSolutions = await getRequestedSolution() }
    catch (error) { throw new Error(String(error)) }

    const storedTheme = localStorage.getItem("theme")
    if (!storedTheme) {
        localStorage.setItem("theme", "theme-dark")
        document.querySelector("html")?.classList.add("theme-dark")
    }
    else { document.querySelector("html")?.classList.add(storedTheme) }

    SEARCH_BOX = c_searchBox()
    ROOT = c_root()

    document.body.replaceChildren(ROOT)

    try { TAGS = await getTags() }
    catch (error) { throw new Error(String(error)) }

    try { FILTERS = await c_filters() }
    catch (error) { throw new Error(String(error)) }

    NAVBAR = c_navbar()
}

initApp()
