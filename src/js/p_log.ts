import { NAVBAR, ROOT } from "./index";
import { UI_TXT, capitalize, lang } from "./u_language";
import { c_log } from "./c_log";

export async function p_log() {
	ROOT.replaceChildren(NAVBAR, await c_log())
	document.title = `Service App | ${capitalize(UI_TXT.log[lang], "first")}`;
}
