import { t_button, t_div } from "./u_html-tags";

export function c_dropdown(text: string, cl: Array<string>, options?: Array<Element | null>) {
	const container = t_div({ class: ["dropdown", "button", "btn--normal"] })
	const dropdown = t_div({ class: "dropdown__menu" })
	const button = t_button({
		class: ["dropdown__btn", "btn", "btn--gray"],
		text: text
	})
	container.classList.add(...cl);

	container.addEventListener("mouseleave", () => {
		dropdown.style.display = "none"
	})

	button.addEventListener("click", () => {
		const display = dropdown.style.display
		if (display === "grid") {
			dropdown.style.display = "none"
			return
		}
		dropdown.style.display = "grid"
	})

	if (options) {
		for (let i = 0; i < options.length; i++) {
			const option = options[i]
			if (!option) { continue }
			dropdown.append(option)
		}
	}

	container.append(button, dropdown)
	return container
}
