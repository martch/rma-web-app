import { UI_TXT, getUserLang } from "./u_language";
import { t_button, t_form, t_input, t_label } from "./u_html-tags";
import { login } from "./sdk/pocketbase/auth";

const lang = getUserLang();

export function c_loginForm() {
    const form = t_form({ id: "login_form" }, [
        t_label({ for: "email", text: `${UI_TXT.email[lang]}:` }),
        t_input({
            type: "email",
            id: "email",
            name: "email",
            attr: { "autocomplete": "email", }
        }),
        t_label({ for: "password", text: `${UI_TXT.password[lang]}:` }),
        t_input({
            type: "password",
            id: "password",
            name: "password",
            attr: { "autocomplete": "password", }
        }),
        t_button({
            id: "login_submit_btn",
            text: UI_TXT.login[lang],
            onclick: () => { attemptLogin(form) }
        }),
    ])

    return form
}

async function attemptLogin(form: HTMLFormElement) {
    try { await login(form.email.value, form.password.value) }
    catch (error) {
        console.error(error)
        const errorMsg = document.createElement("div")
        errorMsg.innerHTML = String(error)
        form.append(errorMsg)
        form.reset()
    }
}
