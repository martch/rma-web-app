import { c_loginForm } from "./c_login-form";

export function buildLoginPage() {
    document.body.replaceChildren(c_loginForm());
    document.title = "login";
}
