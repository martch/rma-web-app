import { t_button } from "./u_html-tags"

export function createTag(name: string, color: string) {
    const el = t_button({ class: ["tag"], text: name })
    el.style.borderColor = `var(--${color}-medium)`
    return el
}
