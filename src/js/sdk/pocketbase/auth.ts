import { RecordAuthResponse, RecordModel } from "pocketbase";
import { PB } from "./init";

export async function login(email: string, password: string) {
    try {
        await PB.collection('users').authWithPassword(email, password);
    } catch (error) {
        throw new Error(`Failed to login. Error: ${error}`)
    }
}

export async function logout() {
    try { PB.authStore.clear(); }
    catch (error) { throw new Error(`Failed to logout. Error: ${error}`) }
}

export function isAuthValid() {
    return PB.authStore.isValid
}

export async function refreshAuth() {
    let response: RecordAuthResponse<RecordModel>
    try {
        response = await PB.collection('users').authRefresh()
        console.log(response)
    }
    catch (error) {
        PB.authStore.clear()
        throw new Error(`Failed to refresh the current authenticated record instance. Error: ${error}`)
    }
}
