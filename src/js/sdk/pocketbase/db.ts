import { PB } from "./init";
import { refreshAuth } from "./auth";
import { ProductData, RequestedSolution, SearchResult, RmaCaseData, ServiceProvider, Supplier, Tag, LogRecord, UserRecord, FilterState, isProductData } from "../../interface"
import { ListResult, RecordModel } from "pocketbase";

export async function updateUserData(id: string, data: object) {
    try {
        await PB.collection('users').update(id, data);
    } catch (error) {
        console.error(`Failed to update user's data. Error: ${error}`);
    }
    try { refreshAuth() }
    catch (error) { console.error(error); }
}

export async function getAllUserNames() {
    try {
        const records = await PB.collection('users').getFullList({
            fields: "id, name",
            sort: '-created',
        })
        return records as Array<UserRecord>
    } catch (error) {
        throw new Error(`Failed to get user names. Error: ${error}`);
    }
}

export async function getSuppliers() {
    try {
        const records = await PB.collection('supplier').getFullList({
            sort: '-created',
        })
        return records as Array<Supplier>
    } catch (error) {
        throw new Error(`Failed to get supplier data. Error: ${error}`)
    }
}

export async function getServiceProviders() {
    try {
        const records = await PB.collection('service_provider').getFullList({
            sort: '-created',
        })
        return records as Array<ServiceProvider>
    } catch (error) {
        throw new Error(`Failed to get service provider data. Error: ${error}`)
    }
}

export async function getRequestedSolution() {
    try {
        const records = await PB.collection('requested_solution').getFullList({
            sort: '-created',
        })
        return records as Array<RequestedSolution>
    } catch (error) {
        throw new Error(`Failed to get requested solution data. Error: ${error}`)
    }
}

export async function getTags() {
    try {
        const records = await PB.collection('tags').getFullList({
            sort: 'created',
        })
        return records as Array<Tag>
    } catch (error) {
        throw new Error(`Failed to get tags data. Error: ${error}`)
    }
}

export async function getNewCaseId() {
    const OFFSET = 0 // set this to the last case id from the old rma app
    let list: ListResult<RecordModel>
    try { list = await PB.collection('rma_cases').getList(1, 1) }
    catch (error) { throw new Error(`Failed to get a list of rma cases. Error: ${error}`) }
    return list.totalItems + 1 + OFFSET
}

export async function addNewCase(data: RmaCaseData) {
    let productDataRecordIds = []
    if (data.product_data) {
        for (let i = 0; i < data.product_data.length; i++) {
            try {
                const productData = data.product_data[i] as ProductData
                const response = await PB.collection("product_data").create(productData)
                productDataRecordIds.push(response.id)
            } catch (error) {
                throw new Error(`Failed to create product_data record. Error: ${error}`)
            }
        }
    }

    // After the records in the product_data collection have been created
    // we can replace the product_data value in the data object with just
    // references to the records in the product_data collection
    // because in the rma_cases collection we have a multiple relation field
    // for products that lead to records in the product_data collection
    data.product_data = productDataRecordIds

    try {
        const response = await PB.collection("rma_cases").create(data)
        return response.id
    } catch (error) {
        throw new Error(`Failed to create rma_cases record. Error: ${error}`)
    }
}

export async function getTableData(filterState: FilterState) {
    const productFilterExpression = (() => {
        if (filterState.supplier === ""
            && filterState.service_provider === ""
            && filterState.tags.includes("tags_all")
        ) { return "" }

        if (filterState.supplier !== ""
            && filterState.service_provider === ""
            && filterState.tags.includes("tags_all")
        ) { return `supplier = "${filterState.supplier}"` }

        if (filterState.supplier === ""
            && filterState.service_provider !== ""
            && filterState.tags.includes("tags_all")
        ) { return `service_provider = "${filterState.service_provider}"` }

        if (filterState.supplier !== ""
            && filterState.service_provider !== ""
            && filterState.tags.includes("tags_all")
        ) {
            return `supplier = "${filterState.supplier}" && service_provider = "${filterState.service_provider}"`
        }

        if (filterState.supplier === ""
            && filterState.service_provider === ""
            && !filterState.tags.includes("tags_all")
        ) {
            const tags = filterState.tags
            let filter = ""
            for (let i = 0; i < tags.length; i++) {
                filter += `tags ~ "${tags[i]}"`
                if (i < tags.length - 1) { filter += " && " }
            }
            return filter
        }

        if (filterState.supplier !== ""
            && filterState.service_provider === ""
            && !filterState.tags.includes("tags_all")
        ) {
            let filter = `supplier = "${filterState.supplier}"`
            const tags = filterState.tags
            for (let i = 0; i < tags.length; i++) {
                filter += ` && tags ~ "${tags[i]}"`
            }
            return filter
        }

        if (filterState.supplier === ""
            && filterState.service_provider !== ""
            && !filterState.tags.includes("tags_all")
        ) {
            let filter = `service_provider = "${filterState.service_provider}"`
            const tags = filterState.tags
            for (let i = 0; i < tags.length; i++) {
                filter += ` && tags ~ "${tags[i]}"`
            }
            return filter
        }

        if (filterState.supplier !== ""
            && filterState.service_provider !== ""
            && !filterState.tags.includes("tags_all")
        ) {
            let filter = `supplier = "${filterState.supplier}" && service_provider = "${filterState.service_provider}"`
            const tags = filterState.tags
            for (let i = 0; i < tags.length; i++) {
                filter += ` && tags ~ "${tags[i]}"`
            }
            return filter
        }
    })()

    try {
        const result = await PB.collection("product_data").getFullList({
            filter: productFilterExpression,
            sort: '-created',
            fields: "id"
        })

        if (result.length === 0) { return [] }

        let caseFilterExpression = ""
        for (let i = 0; i < result.length; i++) {
            caseFilterExpression += `product_data ~ "${result[i].id}"`
            if (i < result.length - 1) {
                caseFilterExpression += " || "
            }
        }

        const records = await PB.collection('rma_cases').getFullList({
            filter: caseFilterExpression,
            sort: '-created',
            expand: "product_data",
            fields: "id, case_id, legal_entity_name, customer_name, created, updated, expand.product_data.product_name, expand.product_data.product_sn, expand.product_data.id",
        })
        return records as Array<RmaCaseData>
    } catch (error) {
        throw new Error(`Failed to get table data. Error: ${error}`)
    }
}

export async function getCaseData(id: string) {
    try {
        const record = await PB.collection('rma_cases').getOne(id, {
            expand: "product_data"
        })
        return record as RmaCaseData
    } catch (error) {
        throw new Error(`Failed to get case data. Error: ${error}`)
    }
}

export async function getCaseDataCommon(id: string) {
    try {
        const record = await PB.collection('rma_cases').getOne(id)
        return record as RmaCaseData
    } catch (error) {
        throw new Error(`Failed to get common case data. Error: ${error}`)
    }
}

export async function getCaseDataProduct(id: string) {
    try {
        const record = await PB.collection('product_data').getOne(id)
        if (isProductData(record)) return record
        else throw new Error(`Returned database record is not of type ProductData.`)
    } catch (error) {
        throw new Error(`Failed to get product data. Error: ${error}`)
    }
}

export async function getRecordUpdated(collection: string, recordId: string) {
    try {
        const record = await PB.collection(collection).getOne(recordId, {
            fields: "updated"
        })
        return record.updated
    } catch (error) {
        throw new Error(`Failed to get "updated" field data. Error: ${error}`)
    }
}

export async function setCaseData(
    collection: string,
    recordId: string,
    data: {
        field_name: string
        field_value: string
    }) {
    try {
        await PB.collection(collection).update(
            recordId,
            { [data.field_name]: data.field_value })
    } catch (error) {
        throw new Error(`Failed to set case data. Error: ${error}`)
    }
}

// TODO: add more fields by which to search
export async function getSearchResults(searchString: string) {
    const searchResults: Array<SearchResult> = []
    try {
        const result = await PB.collection("product_data").getList(1, 50, {
            filter: PB.filter("product_name ~ {:s} || product_sn ~ {:s}",
                { s: searchString }),
            fields: "id, product_name, product_sn "
        })
        for (let i = 0; i < result.items.length; i++) {
            const item = result.items[i]
            const searchResult: SearchResult = {
                type: "product_name",
                case_id: "",
                id: "",
                value: "",
            }
            Object.keys(item).forEach((key) => {
                if (String(item[key]).includes(searchString)) {
                    if (key === "product_name"
                        || key === "product_sku"
                        || key === "product_sn") searchResult.type = key
                    searchResult.value = item[key]
                }
            })
            try {
                const result = await PB.collection("rma_cases").getList(1, 1, {
                    filter: `product_data ~ "${item.id}"`,
                    fields: "id, case_id"
                })
                searchResult.case_id = result.items[0].case_id
                searchResult.id = result.items[0].id
            } catch (error) {
                throw new Error(`Failed to get case id. Error: ${error}`)
            }
            searchResults.push(searchResult)

            if (i === result.items.length - 1) {
                return searchResults
            }
        }
    }
    catch (error) {
        throw new Error(`Failed to get search results. Error: ${error}`)
    }
}

export async function addLogRecord(record: LogRecord) {
    try {
        return await PB.collection('log').create(record) as LogRecord
    } catch (error) {
        throw new Error(`Failed to add log record. Error: ${error}`)
    }
}

export async function getLogs(productId: string) {
    try {
        const records: Array<LogRecord> = await PB.collection('log').getFullList({
            filter: `product_id = "${productId}"`,
            sort: 'created',
            fields: "id, type, user_id, tag, tag_id, comment, created"
        })
        return records
    } catch (error) {
        throw new Error(`Failed to get logs. Error: ${error}`)
    }
}

export async function getActiveTagIds(productId: string) {
    try {
        const record: ProductData = await PB
            .collection('product_data')
            .getOne(productId, {
                fields: "tags"
            })
        if (!record.tags) { return [] }
        return record.tags
    } catch (error) {
        throw new Error(`Failed to get product tags. Error: ${error}`)
    }
}

export async function setActiveTags(productId: string, tagIds: Array<string>) {
    try {
        await PB
            .collection("product_data")
            .update(productId, { tags: tagIds }) as ProductData
    } catch (error) {
        throw new Error(`Failed to set active product tags. Error: ${error}`)
    }
}
