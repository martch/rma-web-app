import PocketBase from 'pocketbase';

export interface PB_UserRecord {
    avatar: string,
    collectionId: string,
    collectionName: string,
    created: string,
    email: string,
    emailVisibility: false,
    id: string,
    name: string,
    role: string,
    updated: string,
    username: string,
    verified: true
}

export const PB = new PocketBase();

PB.autoCancellation(false)
