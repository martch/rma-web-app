import { RmaCaseData } from "./interface"

(() => {
    const storedData = sessionStorage.getItem("case_data")
    if (!storedData) {
        console.error("Missing data. Aborting operation.")
        return
    }

    const data: RmaCaseData = JSON.parse(storedData)
    if (!data.expand) {
        console.error("Data is missing the 'expand' property. Aborting operation.")
        return
    }

    const productData = data.expand.product_data
    if (!productData) {
        console.error("Missing product data.")
        return
    }

    const type = document.body.id

    switch (type) {
        case "registration_form":
            document.title = `RMA#${data.case_id} registration form`
        case "release_form":
            document.title = `RMA#${data.case_id} release form`
        case "return_form":
            document.title = `RMA#${data.case_id} return form`
    }

    const fieldCaseId = document.querySelector("#case_id")
    if (fieldCaseId) { fieldCaseId.innerHTML = String(data.case_id); }

    const fieldCaseCreated = document.querySelector("#case_added_timestamp")
    if (fieldCaseCreated) {
        if (!data.created) { fieldCaseCreated.innerHTML = "" }
        else {
            fieldCaseCreated.innerHTML = new Intl
                .DateTimeFormat("lv")
                .format(new Date(data.created))
        }
    }

    const fieldClientName = document.querySelector("#client_name")
    if (fieldClientName) {
        fieldClientName.innerHTML = String(data.customer_name)
    }

    const fieldClientCompany = document.querySelector("#client_company")
    if (fieldClientCompany) {
        fieldClientCompany.innerHTML = String(data.legal_entity_name)
    }

    const fieldClientPhone = document.querySelector("#client_phone")
    if (fieldClientPhone) {
        fieldClientPhone.innerHTML = String(data.customer_phone)
    }

    const fieldClientEmail = document.querySelector("#client_email")
    if (fieldClientEmail) {
        fieldClientEmail.innerHTML = String(data.customer_email)
    }

    const fieldClientOrderId = document.querySelector("#client_order_id")
    if (fieldClientOrderId) {
        fieldClientOrderId.innerHTML = String(data.customer_order_id)
    }

    const fieldOrderDate = document.querySelector("#client_order_date")
    if (fieldOrderDate) {
        if (!data.customer_order_date) { fieldOrderDate.innerHTML = "" }
        else {
            fieldOrderDate.innerHTML = new Intl
                .DateTimeFormat("lv")
                .format(new Date(data.customer_order_date))
        }
    }

    const fieldClientBankAccount = document.querySelector("#client_bank_account")
    if (fieldClientBankAccount) {
        fieldClientBankAccount.innerHTML = String(data.customer_bank_account)
    }

    const productNotes: Array<string> = []
    const productName: Array<string> = []
    const productSn: Array<string> = []
    const productSet: Array<string> = []
    const productPrice: Array<number> = []
    const returnReason: Array<string> = []
    const testResults: Array<string> = []
    const solution: Array<string> = []

    for (let i = 0; i < productData.length; i++) {
        const notes = productData[i].product_notes
        if (notes) { productNotes.push(notes) }
        const name = productData[i].product_name
        if (name) { productName.push(name) }
        const sn = productData[i].product_sn
        if (sn) { productSn.push(sn) }

        // TODO: implement product set
        const set = productData[i].product_sn
        if (set) { productSet.push(set) }

        const price = productData[i].product_price
        if (price) { productPrice.push(price) }

        const ret = productData[i].product_return_reason
        if (ret) { returnReason.push(ret) }

        const results = productData[i].product_return_reason
        if (results) { testResults.push(results) }

        const sol = productData[i].product_return_reason
        if (sol) { solution.push(sol) }
    }


    const fieldNotes = document.querySelector("#notes")
    const fieldProductName = document.querySelector("#product_name")
    const fieldProductSn = document.querySelector("#product_sn")
    const fieldProductSet = document.querySelector("#product_set")
    const fieldProductPrice = document.querySelector("#product_price")
    const fieldProblemDescr = document.querySelector("#problem_description")
    const fieldTestResults = document.querySelector("#test_results")
    const fieldSolution = document.querySelector("#solution")
    if (fieldNotes) { fieldNotes.innerHTML = productNotes.join(" // ") }
    if (fieldProductName) { fieldProductName.innerHTML = productName.join(" // ") }
    if (fieldProductSn) { fieldProductSn.innerHTML = productSn.join(" // ") }
    if (fieldProductSet) { fieldProductSet.innerHTML = productSet.join(" // ") }
    if (fieldProductPrice) { fieldProductPrice.innerHTML = productPrice.join(" // ") }
    if (fieldProblemDescr) { fieldProblemDescr.innerHTML = returnReason.join(" // ") }
    if (fieldTestResults) { fieldTestResults.innerHTML = testResults.join(" // ") }
    if (fieldSolution) { fieldSolution.innerHTML = solution.join(" // ") }

    const fieldCreatedBy = document.querySelector("#username")
    if (fieldCreatedBy) { fieldCreatedBy.innerHTML = String(data.created_by) }
})()
