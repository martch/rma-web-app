import { getServiceProviders, getSuppliers } from "./sdk/pocketbase/db"
import { FilterState } from "./interface"
import { t_button, t_div, t_fieldset, t_input, t_label, t_legend, t_option, t_select } from "./u_html-tags"
import { UI_TXT, capitalize, getUserLang } from "./u_language"
import { route } from "./u_router"
import { TAGS } from "."

const lang = getUserLang()

export async function c_filters() {
	const suppliersSelector = await suppliers()
	const serviceProvidersSelector = await serviceProviders()
	const tags = buildTags()

	const resetFilter = () => {
		suppliersSelector.selectedIndex = 0
		serviceProvidersSelector.selectedIndex = 0
		const allTags = tags.getElementsByClassName("tag_checkbox")
		for (let i = 0; i < allTags.length; i++) {
			const tag = allTags[i] as HTMLInputElement
			if (tag.id === "tags_all") { tag.checked = true }
			else { tag.checked = false }
		}
	}

	const filters = t_div({ class: "filters", }, [
		t_label({
			for: "filter_supplier",
			text: capitalize(UI_TXT.supplier[lang], "first")
		}),
		suppliersSelector,
		t_label({
			for: "filter_service_provider",
			text: capitalize(UI_TXT.service_provider[lang], "first")
		}),
		serviceProvidersSelector,
		t_label({
			for: "filter_tags",
			text: capitalize(UI_TXT.tags[lang], "first")
		}),
		tags,
		t_button({
			text: capitalize(UI_TXT.reset[lang], "first"),
			onclick: () => {
				resetFilter()
				route(null, "/main")
			}
		}),
		t_button({
			text: capitalize(UI_TXT.filter[lang], "first"),
			onclick: () => { route(null, "/main") }
		})])

	return filters
}

async function suppliers() {
	const el = t_select({
		id: `filter_supplier`,
		name: `filter_supplier`,
	}, [
		t_option({ value: "all", text: UI_TXT.all[lang] })
	])

	try {
		const suppliers = await getSuppliers()
		for (let i = 0; i < suppliers.length; i++) {
			el.append(t_option({
				value: suppliers[i].id,
				text: suppliers[i].value,
			}));
		}
	} catch (error) {
		console.error(error)
	}

	return el
}

async function serviceProviders() {
	const el = t_select({
		id: `filter_service_provider`,
		name: `filter_service_provider`,
	}, [
		t_option({ value: "all", text: UI_TXT.all[lang] })
	])

	try {
		const serviceProviders = await getServiceProviders()

		for (let i = 0; i < serviceProviders.length; i++) {
			el.append(t_option({
				value: serviceProviders[i].id,
				text: serviceProviders[i].value,
			}));
		}
	} catch (error) {
		console.error(error)
	}

	return el
}


function buildTags() {
	const el = t_fieldset({ id: "filter_tags" }, [
		t_legend({ text: "tagi" }),
		t_input({
			type: "checkbox",
			id: "tags_all",
			class: "tag_checkbox",
			name: "tags_all",
			attr: { "checked": "checked" },
			oninput: () => {
				const allInputs = document
					.getElementById("filter_tags")
					?.getElementsByTagName("input")
				if (!allInputs) return
				for (let i = 0; i < allInputs.length; i++) {
					const input = allInputs[i]
					if (input.id !== "tags_all") continue
					if (input.checked) {
						for (let i = 0; i < allInputs.length; i++) {
							const input = allInputs[i]
							if (input.id === "tags_all") continue
							input.checked = false
						}
					}
					return
				}
			}
		}),
		t_label({ for: "tags_all", text: UI_TXT.all[lang] }),
	])
	for (let i = 0; i < TAGS.length; i++) {
		const chbx = t_input({
			type: "checkbox",
			id: TAGS[i].id,
			class: "tag_checkbox",
			name: TAGS[i].id,
			oninput: () => {
				const allInputs = document
					.getElementById("filter_tags")
					?.getElementsByTagName("input")
				if (!allInputs) return
				for (let i = 0; i < allInputs.length; i++) {
					const input = allInputs[i]
					if (input.id === "tags_all") input.checked = false
					return
				}
			}
		})
		const lbl = t_label({ for: TAGS[i].id, text: TAGS[i].value[lang] })
		el.append(chbx, lbl)
	}
	return el
}

export function u_getFilterState(filter: HTMLDivElement) {
	const supplier = filter
		.getElementsByTagName("select")
		.namedItem("filter_supplier")
	const serviceProvider = filter
		.getElementsByTagName("select")
		.namedItem("filter_service_provider")
	const tags = filter
		.getElementsByClassName("tag_checkbox")
	if (!supplier || !serviceProvider || !tags) {
		throw new Error(`supplier is ${JSON.stringify(supplier)}\n
			serviceProvider is ${JSON.stringify(serviceProvider)}\n
			tags is ${JSON.stringify(tags)}\n `)
	}

	let selectedSupplier = supplier.options[supplier.selectedIndex].value
	if (selectedSupplier === "all") selectedSupplier = ""

	let selectedServiceProvider = serviceProvider
		.options[serviceProvider.selectedIndex]
		.value
	if (selectedServiceProvider === "all") selectedServiceProvider = ""

	let selectedTags: Array<string> = []
	for (let i = 0; i < tags.length; i++) {
		const tag = tags[i] as HTMLInputElement
		if (tag.checked) selectedTags.push(tag.id)
	}

	const filterState: FilterState = {
		supplier: selectedSupplier,
		service_provider: selectedServiceProvider,
		tags: selectedTags,
	}

	return filterState
}
