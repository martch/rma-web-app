// https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
export interface LanguageOptions {
    lav: string;
    rus: string;
    eng: string;
}

interface UIText {
    case_form: {
        added: LanguageOptions
        updated: LanguageOptions
        case_id: LanguageOptions
        legal_entity: LanguageOptions
        customer_name: LanguageOptions
        phone: LanguageOptions
        email: LanguageOptions
        order: LanguageOptions
        order_date: LanguageOptions
        bank_account: LanguageOptions
        product_name: LanguageOptions
        product_sn: LanguageOptions
        product_sku: LanguageOptions
        product_price: LanguageOptions
        product_cost: LanguageOptions
        product_notes: LanguageOptions
        return_reason: LanguageOptions
        requested_solution: LanguageOptions
        tags: LanguageOptions
    }
    add: LanguageOptions
    add_product: LanguageOptions
    admin: LanguageOptions
    administration: LanguageOptions
    all: LanguageOptions
    bank_account: LanguageOptions
    cancel: LanguageOptions
    case_added_by: LanguageOptions
    case_added_timestamp: LanguageOptions
    case_finished_timestamp: LanguageOptions
    case_id: LanguageOptions
    changelog: LanguageOptions
    client_bank_account: LanguageOptions
    client_company: LanguageOptions
    client_credit_note: LanguageOptions
    client_email: LanguageOptions
    client_name: LanguageOptions
    client_order_date: LanguageOptions
    client_order_id: LanguageOptions
    client_phone: LanguageOptions
    close: LanguageOptions
    commented: LanguageOptions
    company: LanguageOptions
    contact_info: LanguageOptions
    customer: LanguageOptions
    display_name: LanguageOptions
    email: LanguageOptions
    filter: LanguageOptions
    general_info: LanguageOptions
    inc: LanguageOptions
    id: LanguageOptions
    journal: LanguageOptions
    last_change: LanguageOptions
    last_name: LanguageOptions
    log: LanguageOptions
    login: LanguageOptions
    logout: LanguageOptions
    main: LanguageOptions
    manage_consta: LanguageOptions
    n_a: LanguageOptions
    name: LanguageOptions
    new_case: LanguageOptions
    notes: LanguageOptions
    options: LanguageOptions
    order: LanguageOptions
    order_date: LanguageOptions
    order_id: LanguageOptions
    overview: LanguageOptions
    password: LanguageOptions
    phone_number: LanguageOptions
    photos: LanguageOptions
    price: LanguageOptions
    print: LanguageOptions
    problem_description: LanguageOptions
    product: LanguageOptions
    product_plural: LanguageOptions
    product_cost: LanguageOptions
    product_index: LanguageOptions
    product_name: LanguageOptions
    product_price: LanguageOptions
    product_set: LanguageOptions
    product_sku: LanguageOptions
    product_sn: LanguageOptions
    profile: LanguageOptions
    refund: LanguageOptions
    registration_form: LanguageOptions
    release_form: LanguageOptions
    remove_product: LanguageOptions
    repair: LanguageOptions
    replacement: LanguageOptions
    reports: LanguageOptions
    requested_solution: LanguageOptions
    request_status_update: LanguageOptions
    reset: LanguageOptions
    returned_item_order: LanguageOptions
    return_form: LanguageOptions
    return_reason: LanguageOptions
    role: LanguageOptions
    save: LanguageOptions
    search: LanguageOptions
    service_provider_case_id: LanguageOptions
    service_provider: LanguageOptions
    service_providers: LanguageOptions
    settings: LanguageOptions
    solution: LanguageOptions
    status: LanguageOptions
    supplier_invoice_date: LanguageOptions
    supplier_invoice_id: LanguageOptions
    supplier: LanguageOptions
    suppliers: LanguageOptions
    swap_order: LanguageOptions
    tag: LanguageOptions
    tags: LanguageOptions
    test_results: LanguageOptions
    transport: LanguageOptions
    type: LanguageOptions
    uid: LanguageOptions
    unknown: LanguageOptions
    url: LanguageOptions
    user: LanguageOptions
    username: LanguageOptions
    user_password: LanguageOptions
    values: LanguageOptions
}

export const UI_TXT: UIText = {
    case_form: {
        added: {
            lav: "Pievienots",
            rus: "WARN: not implemented",
            eng: "Added",
        },
        updated: {
            lav: "Atjaunots",
            rus: "WARN: not implemented",
            eng: "Updated",
        },
        case_id: {
            lav: "RMA#",
            rus: "WARN: not implemented",
            eng: "RMA#",
        },
        legal_entity: {
            lav: "Juridiskās personas nosaukums",
            rus: "WARN: not implemented",
            eng: "Legal entity name",
        },
        customer_name: {
            lav: "Klienta vārds un uzvārds",
            rus: "WARN: not implemented",
            eng: "Customer's first and last name",
        },
        phone: {
            lav: "Klienta telefona numurs",
            rus: "WARN: not implemented",
            eng: "Customer's phone number",
        },
        email: {
            lav: "Klienta epasts",
            rus: "WARN: not implemented",
            eng: "Customer's email",
        },
        order: {
            lav: "Pasūtījuma numurs",
            rus: "WARN: not implemented",
            eng: "Order number",
        },
        order_date: {
            lav: "Pasūtījuma grāmatošanas datums",
            rus: "WARN: not implemented",
            eng: "Order completed date",
        },
        bank_account: {
            lav: "Bankas konta numurs",
            rus: "WARN: not implemented",
            eng: "Bank account number",
        },
        product_name: {
            lav: "Preces nosaukums",
            rus: "WARN: not implemented",
            eng: "Product's description",
        },
        product_sn: {
            lav: "Preces sērijas numurs",
            rus: "WARN: not implemented",
            eng: "Product's serial number",
        },
        product_sku: {
            lav: "Krājuma kods",
            rus: "WARN: not implemented",
            eng: "Product's SKU number",
        },
        product_price: {
            lav: "Preces cena",
            rus: "WARN: not implemented",
            eng: "Product's price",
        },
        product_cost: {
            lav: "Preces izmaksas",
            rus: "WARN: not implemented",
            eng: "Product's cost",
        },
        product_notes: {
            lav: "Piezīmes",
            rus: "WARN: not implemented",
            eng: "Notes",
        },
        return_reason: {
            lav: "Atgriešanas iemesls",
            rus: "WARN: not implemented",
            eng: "Return reason",
        },
        requested_solution: {
            lav: "Vēlamais risinājums",
            rus: "WARN: not implemented",
            eng: "Preferred solution",
        },
        tags: {
            lav: "Tagi",
            rus: "WARN: not implemented",
            eng: "Tags",
        },
    },
    add: {
        lav: "pievienot",
        rus: "WARN: not implemented",
        eng: "add",
    },
    add_product: {
        lav: "pievienot preci",
        rus: "WARN: not implemented",
        eng: "add product",
    },
    admin: {
        lav: "administrators",
        rus: "WARN: not implemented",
        eng: "admin",
    },
    administration: {
        lav: "administrācija",
        rus: "WARN: not implemented",
        eng: "administration",
    },
    all: {
        lav: "visi",
        rus: "WARN: not implemented",
        eng: "all",
    },
    bank_account: {
        lav: "bankas konts",
        rus: "WARN: not implemented",
        eng: "bank account",
    },
    cancel: {
        lav: "atcelt",
        rus: "WARN: not implemented",
        eng: "cancel",
    },
    case_added_by: {
        lav: "kas pievienoja",
        rus: "WARN: not implemented",
        eng: "case added by",
    },
    case_added_timestamp: {
        lav: "kad pievienots",
        rus: "WARN: not implemented",
        eng: "case added timestamp",
    },
    case_finished_timestamp: {
        lav: "kad pabeigts",
        rus: "WARN: not implemented",
        eng: "case finished timestamp",
    },
    case_id: {
        lav: "darba id",
        rus: "WARN: not implemented",
        eng: "case id",
    },
    changelog: {
        lav: "izmaiņu žurnāls",
        rus: "WARN: not implemented",
        eng: "changelog",
    },
    customer: {
        lav: "klients",
        rus: "WARN: not implemented",
        eng: "client",
    },
    client_bank_account: {
        lav: "klienta bankas ko",
        rus: "WARN: not implemented",
        eng: "client's bank account",
    },
    client_company: {
        lav: "klienta uzņēmums",
        rus: "WARN: not implemented",
        eng: "client's company",
    },
    client_credit_note: {
        lav: "klienta kredītrēķins",
        rus: "WARN: not implemented",
        eng: "client's crredit note",
    },
    client_email: {
        lav: "klienta e-pasts",
        rus: "WARN: not implemented",
        eng: "client's e-mail",
    },
    client_name: {
        lav: "klienta vārds",
        rus: "WARN: not implemented",
        eng: "client's name",
    },
    client_order_date: {
        lav: "klienta ppr datums",
        rus: "WARN: not implemented",
        eng: "client order's date",
    },
    client_order_id: {
        lav: "klienta ppr",
        rus: "WARN: not implemented",
        eng: "client order's id",
    },
    client_phone: {
        lav: "klienta telefons",
        rus: "WARN: not implemented",
        eng: "client's phone",
    },
    close: {
        lav: "aizvērt",
        rus: "WARN: not implemented",
        eng: "close",
    },
    commented: {
        lav: "komentēja",
        rus: "WARN: not implemented",
        eng: "commented",
    },
    company: {
        lav: "uzņēmums",
        rus: "WARN: not implemented",
        eng: "company",
    },
    contact_info: {
        lav: "kontakta informācija",
        rus: "WARN: not implemented",
        eng: "contact information",
    },
    display_name: {
        lav: "redzamais vārds",
        rus: "WARN: not implemented",
        eng: "display name",
    },
    email: {
        lav: "e-pasts",
        rus: "WARN: not implemented",
        eng: "e-mail",
    },
    filter: {
        lav: "atlasīt",
        rus: "WARN: not implemented",
        eng: "filter",
    },
    general_info: {
        lav: "vispārēja informācija",
        rus: "WARN: not implemented",
        eng: "general information",
    },
    inc: {
        lav: "sia",
        rus: "WARN: not implemented",
        eng: "inc.",
    },
    id: {
        lav: "id",
        rus: "WARN: not implemented",
        eng: "id",
    },
    journal: {
        lav: "žurnāls",
        rus: "WARN: not implemented",
        eng: "journal",
    },
    last_change: {
        lav: "pēdējās izmaiņas",
        rus: "WARN: not implemented",
        eng: "last change",
    },
    last_name: {
        lav: "uzvārds",
        rus: "WARN: not implemented",
        eng: "last name",
    },
    log: {
        lav: "žurnāls",
        rus: "WARN: not implemented",
        eng: "log",
    },
    login: {
        lav: "pierakstīties",
        rus: "WARN: not implemented",
        eng: "login",
    },
    logout: {
        lav: "izrakstīties",
        rus: "WARN: not implemented",
        eng: "logout",
    },
    main: {
        lav: "galvenā",
        rus: "WARN: not implemented",
        eng: "main",
    },
    manage_consta: {
        lav: "pārvaldīt konstantes",
        rus: "WARN: not implemented",
        eng: "manage consta",
    },
    n_a: {
        lav: "Nav norādīts",
        rus: "WARN: not implemented",
        eng: "Not assigned",
    },
    name: {
        lav: "vārds",
        rus: "WARN: not implemented",
        eng: "name",
    },
    new_case: {
        lav: "jauns rma",
        rus: "WARN: not implemented",
        eng: "new case",
    },
    notes: {
        lav: "piezīmes",
        rus: "WARN: not implemented",
        eng: "notes",
    },
    options: {
        lav: "opcijas",
        rus: "WARN: not implemented",
        eng: "options",
    },
    order_date: {
        lav: "pasūtījuma datums",
        rus: "WARN: not implemented",
        eng: "order date",
    },
    order: {
        lav: "pasūtījums",
        rus: "WARN: not implemented",
        eng: "order",
    },
    order_id: {
        lav: "pasūtījuma numurs",
        rus: "WARN: not implemented",
        eng: "order id",
    },
    overview: {
        lav: "pārskats",
        rus: "WARN: not implemented",
        eng: "overview",
    },
    password: {
        lav: "parole",
        rus: "WARN: not implemented",
        eng: "password",
    },
    phone_number: {
        lav: "telefona numurs",
        rus: "WARN: not implemented",
        eng: "phone number",
    },
    photos: {
        lav: "foto",
        rus: "WARN: not implemented",
        eng: "photos",
    },
    price: {
        lav: "cena",
        rus: "WARN: not implemented",
        eng: "price",
    },
    print: {
        lav: "drukāt",
        rus: "WARN: not implemented",
        eng: "print",
    },
    problem_description: {
        lav: "problēmas apraksts",
        rus: "WARN: not implemented",
        eng: "problem's description",
    },
    product: {
        lav: "prece",
        rus: "WARN: not implemented",
        eng: "product",
    },
    product_plural: {
        lav: "preces",
        rus: "WARN: not implemented",
        eng: "produ",
    },
    product_cost: {
        lav: "preces izmaksas",
        rus: "WARN: not implemented",
        eng: "product's cost",
    },
    product_sku: {
        lav: "krājums",
        rus: "WARN: not implemented",
        eng: "product's id",
    },
    product_index: {
        lav: "preces indekss",
        rus: "WARN: not implemented",
        eng: "product's index",
    },
    product_name: {
        lav: "preces nosaukums",
        rus: "WARN: not implemented",
        eng: "product's name",
    },
    product_price: {
        lav: "preces cena",
        rus: "WARN: not implemented",
        eng: "product's price",
    },
    product_set: {
        lav: "komplektācija",
        rus: "WARN: not implemented",
        eng: "product's set",
    },
    product_sn: {
        lav: "preces sn",
        rus: "WARN: not implemented",
        eng: "product's sn",
    },
    profile: {
        lav: "profils",
        rus: "WARN: not implemented",
        eng: "profile",
    },
    refund: {
        lav: "naudas atmaksa",
        rus: "WARN: not implemented",
        eng: "refund",
    },
    registration_form: {
        lav: "pieņemšanas forma",
        rus: "WARN: not implemented",
        eng: "registration form",
    },
    release_form: {
        lav: "izsniegšanas forma",
        rus: "WARN: not implemented",
        eng: "release form",
    },
    remove_product: {
        lav: "noņemt preci",
        rus: "WARN: not implemented",
        eng: "remove product",
    },
    repair: {
        lav: "remonts",
        rus: "WARN: not implemented",
        eng: "repair",
    },
    replacement: {
        lav: "nomaiņa",
        rus: "WARN: not implemented",
        eng: "replacement",
    },
    reports: {
        lav: "atskaites",
        rus: "WARN: not implemented",
        eng: "reports",
    },
    requested_solution: {
        lav: "vēlamais risinājums",
        rus: "WARN: not implemented",
        eng: "requested solution",
    },
    request_status_update: {
        lav: "pieprasīt statusa atjauninājumu",
        rus: "WARN: not implemented",
        eng: "request status update",
    },
    reset: {
        lav: "atiestatīt",
        rus: "WARN: not implemented",
        eng: "reset",
    },
    returned_item_order: {
        lav: "atgriezto preci izdot",
        rus: "WARN: not implemented",
        eng: "order for returned product",
    },
    return_form: {
        lav: "atteikuma veidlapa",
        rus: "WARN: not implemented",
        eng: "return form",
    },
    return_reason: {
        lav: "atgriešanas iemesls",
        rus: "WARN: not implemented",
        eng: "return reason",
    },
    role: {
        lav: "loma",
        rus: "WARN: not implemented",
        eng: "role",
    },
    save: {
        lav: "saglabāt",
        rus: "WARN: not implemented",
        eng: "save",
    },
    search: {
        lav: "meklēt",
        rus: "WARN: not implemented",
        eng: "search",
    },
    service_provider_case_id: {
        lav: "servisa darba id",
        rus: "WARN: not implemented",
        eng: "service provider case id",
    },
    service_provider: {
        lav: "serviss",
        rus: "WARN: not implemented",
        eng: "service provider",
    },
    service_providers: {
        lav: "servisi",
        rus: "WARN: not implemented",
        eng: "service providers",
    },
    settings: {
        lav: "iestatījumi",
        rus: "WARN: not implemented",
        eng: "settings",
    },
    solution: {
        lav: "risinājums",
        rus: "WARN: not implemented",
        eng: "solution",
    },
    status: {
        lav: "statuss",
        rus: "WARN: not implemented",
        eng: "status",
    },
    supplier_invoice_date: {
        lav: "piegādātāja ppr datums",
        rus: "WARN: not implemented",
        eng: "supplier invoice's date",
    },
    supplier_invoice_id: {
        lav: "piegādātāja ppr",
        rus: "WARN: not implemented",
        eng: "supplier invoice's id",
    },
    supplier: {
        lav: "piegādātājs",
        rus: "WARN: not implemented",
        eng: "supplier",
    },
    suppliers: {
        lav: "piegādātāji",
        rus: "WARN: not implemented",
        eng: "suppliers",
    },
    swap_order: {
        lav: "maiņas/piegādes pasūtījums",
        rus: "WARN: not implemented",
        eng: "swap/delivery order",
    },
    tag: {
        lav: "tags",
        rus: "WARN: not implemented",
        eng: "tag",
    },
    tags: {
        lav: "tagi",
        rus: "WARN: not implemented",
        eng: "tags",
    },
    test_results: {
        lav: "pārbaudes rezultāti",
        rus: "WARN: not implemented",
        eng: "test results",
    },
    transport: {
        lav: "transports",
        rus: "WARN: not implemented",
        eng: "transport",
    },
    type: {
        lav: "tips",
        rus: "WARN: not implemented",
        eng: "type",
    },
    uid: {
        lav: "lietotāja id",
        rus: "WARN: not implemented",
        eng: "user's id",
    },
    unknown: {
        lav: "nezināms",
        rus: "WARN: not implemented",
        eng: "unknown",
    },
    url: {
        lav: "url",
        rus: "WARN: not implemented",
        eng: "url",
    },
    user: {
        lav: "lietotājs",
        rus: "WARN: not implemented",
        eng: "user",
    },
    username: {
        lav: "lietotāja vārds",
        rus: "WARN: not implemented",
        eng: "user's name",
    },
    user_password: {
        lav: "lietotāja parole",
        rus: "WARN: not implemented",
        eng: "user's password",
    },
    values: {
        lav: "vērtības",
        rus: "WARN: not implemented",
        eng: "values",
    },
}

export const lang = getUserLang()

// TODO: add option for users to change language, and save the selection in local storage
export function getUserLang() {
    const userLang = localStorage.getItem("userLang") as keyof LanguageOptions
    if (!userLang) {
        localStorage.setItem("userLang", "lav")
        return "lav"
    }
    return userLang
}

export function capitalize(text: string, mode: ("none" | "first" | "all" | "all-caps")): string {
    switch (mode) {
        case "none":
            return text.toLowerCase();
        case "first":
            return text.toLowerCase().charAt(0).toUpperCase() + text.slice(1);
        case "all":
            const textSplit = text.split(" ");
            let result = "";
            for (const word in textSplit) {
                result += " ";
                result +=
                    textSplit[word]
                        .toLowerCase()
                        .charAt(0)
                        .toUpperCase()
                    + textSplit[word].slice(1);
            }
            result.trim();
            return result;
        case "all-caps":
            return text.toUpperCase();
        default:
            return text;
    }
}
