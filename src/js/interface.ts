import { LanguageOptions } from "./u_language"

export interface UserRecord {
    avatar: string
    collectionId: string
    collectionName: string
    created: string
    email: string
    emailVisibility: false
    id: string
    name: string
    role: string
    updated: string
    username: string
    verified: true
}

export interface Supplier {
    collectionId: string
    collectionName: string
    created: string
    id: string
    name: string
    updated: string
    value: string
    website: string
}

export interface ServiceProvider {
    collectionId: string
    collectionName: string
    created: string
    id: string
    name: string
    updated: string
    value: string
    website: string
}

export interface RequestedSolution {
    collectionId: string
    collectionName: string
    created: string
    id: string
    name: string
    updated: string
    value: LanguageOptions
}

export interface Tag {
    id: string
    collectionId: string
    collectionName: string
    created: string
    updated: string
    name: string
    color: "red" | "green" | "blue" | "pink" | "grape" | "violet" | "indigo" | "cyan" | "teal" | "lime" | "yellow" | "orange" | "gray"
    value: LanguageOptions
}

export interface ProductData {
    [key: string]: string
    | number
    | null
    | undefined
    | object
    id?: string
    created?: string
    updated?: string
    product_name?: string | null
    product_sku?: string | null
    product_sn?: string | null
    product_price?: number | null
    product_cost?: number | null
    items_list?: {
        device: boolean
        batteries: boolean
        packaging: boolean
        charger: boolean
        other: string
    } | null
    product_condition?: "unopened" | "new" | "used" | "damaged" | null
    notes_product_condition?: string | null
    password?: string | null
    product_return_reason?: string | null
    product_requested_solution?: "n_a" | "repair" | "replace" | "refund" | null
    product_notes?: string | null
    additional_orders?: {
        type: OrderType
        order: string
    }
    supplier?: Supplier | null
    supplier_invoice_id?: string | null
    supplier_invoice_date?: number | null
    service_provider?: ServiceProvider | null
    service_case_id?: string | null
    test_results?: string | null
    solution?: string | null
    tags?: Array<string>

export function isProductData(test: any): test is ProductData {
    if (test.additional_orders
        && test.created
        && test.id
        && test.items_list
        && test.notes_product_condition
        && test.password
        && test.product_condition
        && test.product_cost
        && test.product_name
        && test.product_notes
        && test.product_price
        && test.product_requested_solution
        && test.product_return_reason
        && test.product_sku
        && test.product_sn
        && test.service_case_id
        && test.service_provider
        && test.solution
        && test.supplier
        && test.supplier_invoice_date
        && test.supplier_invoice_id
        && test.tags
        && test.test_results
        && test.updated
    ) return true
    return false
}

export interface OrderType {
    name: string
    value: JSON
}

export interface AdditionalOrders {
    type: OrderType
    value: string
}

export interface RmaCaseData {
    [key: string]: string
    | number
    | boolean
    | null
    | undefined
    | object
    id?: string
    created?: string
    updated?: string
    case_id?: number
    created_by?: string
    customer_name?: string | null
    customer_phone?: string | null
    customer_email?: string | null
    customer_bank_account?: string | null
    customer_order_id?: string | null
    customer_order_date?: number | null
    legal_entity_name?: string | null
    product_data?: Array<ProductData> | Array<string>
    tags?: Array<Tag> | Array<string>
    expand?: {
        product_data?: Array<ProductData>
        tags?: Array<Tag>
    }
}

export interface FilterState {
    supplier: string
    service_provider: string
    tags: Array<string>
}

export interface SearchResult {
    type:
    "product_name"
    | "product_sku"
    | "product_sn"
    case_id: string
    id: string
    value: string
}

export interface LogRecord {
    id?: string
    product_id?: string
    type?: "comment_add" | "comment_edit" | "tag_add" | "tag_remove"
    user_id?: string
    original_entry_id?: string
    comment?: string
    tag_id?: string
    created?: Date
    updated?: Date
}
