import { getSearchResults } from "./sdk/pocketbase/db";
import { SearchResult } from "./interface";
import { t_a, t_div, t_input } from "./u_html-tags";
import { UI_TXT, lang } from "./u_language";

let timeoutId = 0
const searchDelay = 1000

export function c_searchBox() {
    const searchBox = t_div(
        { class: ["dropdown", "navbar__search"] },
        [
            t_input({
                type: "text",
                class: "navbar__search__input",
                id: "search_input_field",
                attr: { placeholder: `${UI_TXT.search[lang]}...` },
                oninput: async () => {
                    const input = searchBox.getElementsByTagName("input")[0]
                    const results = searchBox.getElementsByTagName("div")[0]
                    await showResults(input, results)
                }
            }),
            t_div({
                class: ["navbar__search__results", "grid"],
                id: "search_results_container"
            }),
        ]
    )

    return searchBox;
}

async function showResults(input: HTMLInputElement, results: HTMLDivElement) {
    clearTimeout(timeoutId)

    timeoutId = window.setTimeout(async () => {
        clearResults(results)
        results.style.display = "grid"

        if (input.value.length === 0) {
            results.style.display = "none"
            return
        }

        if (input.value.length < 4 && isNaN(Number(input.value))) {
            results.classList.add("navbar__search__results--error")
            results.innerHTML = "search parameter must be 4 symbols or more"
            return
        }

        let searchResults: Array<SearchResult> | undefined

        try { searchResults = await getSearchResults(input.value) }
        catch (error) { return console.error(error) }

        if (!searchResults || searchResults.length === 0) {
            results.classList.add("navbar__search__results--info")
            results.innerHTML = "no results :("
            return
        }

        for (let i = 0; i < searchResults.length; i++) {
            results.classList.add("navbar__search__results--confirm");
            const result: SearchResult = searchResults[i];

            const text = `${UI_TXT.case_id[lang]}: ${result.case_id}, ${UI_TXT[result.type][lang]}: ${result.value}`;
            results.append(
                t_a({
                    text: text,
                    href: `/case?id=${result.id}`,
                    useRouter: true,
                })
            )
        }
    }, searchDelay);
}

function clearResults(results: HTMLDivElement) {
    results.classList.remove(
        "navbar__search__results--error",
        "navbar__search__results--warning",
        "navbar__search__results--info",
        "navbar__search__results--confirm")
    results.innerHTML = ""
}
