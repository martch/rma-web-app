import { route } from "./u_router";

export function t_a(opt: {
    class?: string | string[],
    id?: string,
    attr?: { [attr: string]: string },
    text?: string | number,
    href: string,
    onclick?: Function,
    useRouter?: boolean
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("a")
    cl(el, opt.class)
    if (opt.id) { el.id = opt.id }
    if (opt.text) { el.innerText = String(opt.text) }
    const attr = opt.attr
    if (attr) {
        Object.keys(attr).forEach((key) => {
            el.setAttribute(key, attr[key])
        })
    }
    el.href = opt.href
    const func = (e: MouseEvent) => {
        if (opt.useRouter) { route(e, el.href) }
        else if (opt.onclick) { opt.onclick() }
    }
    onclick(el, func)
    addChildren(el, ch)
    return el
}

export function t_button(opt: {
    class?: string | string[],
    id?: string,
    text?: string | number,
    onclick?: Function,
    attr?: { [attr: string]: string },
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("button");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_div(opt: {
    class?: string | string[],
    id?: string,
    onclick?: Function,
    attr?: { [attr: string]: string },
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("div");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_fieldset(opt: {
    class?: string | string[],
    id?: string,
    attr?: { [attr: string]: string },
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("fieldset");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_form(opt: {
    class?: string | string[],
    id?: string
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("form");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_input(opt: {
    type: string,
    class?: string | string[],
    id?: string,
    name?: string,
    value?: string,
    oninput?: Function,
    attr?: { [attr: string]: string }
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("input");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_label(opt: {
    class?: string | string[],
    id?: string,
    for: string,
    text?: string | number
    attr?: { [attr: string]: string },
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("label");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_legend(opt: {
    class?: string | string[],
    id?: string,
    text?: string | number
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("legend");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_nav(opt: {
    class?: string | string[],
    id?: string,
    attr?: { [attr: string]: string },
    onclick?: Function,
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("nav");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_option(opt: {
    class?: string | string[],
    id?: string,
    text?: string | number,
    value?: string,
    selected?: boolean
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("option");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_select(opt: {
    class?: string | string[],
    id?: string,
    name?: string,
    value?: string,
    onclick?: Function,
    attr?: { [attr: string]: string }
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("select");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_table(opt: {
    class?: string | string[],
    id?: string
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("table");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_tbody(opt: {
    class?: string | string[],
    id?: string,
    attr?: { [attr: string]: string }
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("tbody");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_td(opt: {
    class?: string | string[],
    id?: string,
    attr?: { [attr: string]: string }
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("td");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_textarea(opt: {
    class?: string | string[],
    id?: string,
    name?: string,
    value?: string,
    oninput?: Function,
    attr?: { [attr: string]: string }
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("textarea");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_th(opt: {
    class?: string | string[],
    id?: string
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("th");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_thead(opt: {
    class?: string | string[],
    id?: string
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("thead");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

export function t_tr(opt: {
    class?: string | string[],
    id?: string
}, ch?: Array<string | HTMLElement>) {
    const el = document.createElement("tr");
    parseOpt(el, opt);
    addChildren(el, ch);
    return el;
}

function parseOpt(el: HTMLElement, opt: { [prop: string]: any }) {
    cl(el, opt.class);
    if (opt.id) { el.id = opt.id; }
    if (opt.type) { (el as HTMLInputElement).type = opt.type; }
    if (opt.text) { el.innerText = String(opt.text); }
    if (opt.name) { (el as HTMLInputElement | HTMLTextAreaElement).name = opt.name; }
    if (opt.for) { (el as HTMLLabelElement).htmlFor = opt.for; }
    if (opt.value) { (el as HTMLInputElement | HTMLTextAreaElement).value = opt.value; }
    if (opt.selected) { (el as HTMLOptionElement).selected = opt.selected; }
    if (opt.attr) {
        Object.keys(opt.attr).forEach((key) => {
            el.setAttribute(key, opt.attr[key]);
        });
    }
    onclick(el, opt.onclick);
    oninput(el, opt.oninput);
}

function addChildren(el: HTMLElement, ch: Array<string | HTMLElement> | undefined) {
    if (ch) { ch.forEach((c) => { el.append(c); }); }
}

function cl(el: HTMLElement, cl: string | string[] | undefined) {
    if (cl) {
        if (Array.isArray(cl)) {
            cl.forEach((e) => {
                el.classList.add(e);
            });
        }
        else { el.classList.add(cl); }
    }
}

function onclick(el: HTMLElement, func: Function | undefined) {
    if (func) {
        el.addEventListener("click", (e) => {
            e.preventDefault(); func(e);
        });
    }
}

function oninput(el: HTMLElement, func: Function | undefined) {
    if (func) {
        el.addEventListener("input", (e) => {
            e.preventDefault(); func(e);
        });
    }
}
