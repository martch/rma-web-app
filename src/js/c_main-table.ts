import { getActiveTagIds, getTags } from "./sdk/pocketbase/db"
import { ProductData, RmaCaseData } from "./interface"
import { t_a, t_div, t_table, t_tbody, t_td, t_th, t_thead, t_tr } from "./u_html-tags"
import { UI_TXT, lang } from "./u_language"
import { TAGS } from "."
import { createTag } from "./c_tag"

export async function mainTable(data: Array<RmaCaseData>) {
	const body = t_tbody({ class: "rma-table__body", })

	for (let i = 0; i < data.length; i++) {
		const caseId = data[i].case_id
		if (!caseId) {
			throw new Error(`caseId is ${caseId}`)
		}
		let previousCaseId = caseId
		previousCaseId = caseId - 1

		const expandedData = data[i].expand
		if (!expandedData) {
			throw new Error(`expandedData is ${expandedData}`)
		}

		const productDataAll = expandedData.product_data
		if (!productDataAll) {
			throw new Error(`productDataAll is ${productDataAll}`)
		}

		for (let j = 0; j < productDataAll.length; j++) {
			const row = t_tr({ class: ["rma-table__body__row", "grid"] })

			if (caseId === previousCaseId || j === 0) {
				row.append(
					t_a({
						class: ["button", "btn--gray", "btn--tall"],
						text: caseId,
						href: `/case?id=${data[i].id}`,
						useRouter: true
					}),
					t_td({
						class: [
							"rma-table__body__row__cell",
							"text--align-center"]
					}, [String(data[i].legal_entity_name)]),
					t_td({
						class: [
							"rma-table__body__row__cell",
							"text--align-center"]
					}, [String(data[i].customer_name)]),
				)
			} else { row.append(t_td({}), t_td({}), t_td({}),) }

			const productData = productDataAll[j] as ProductData

			const productId = productData.id
			if (!productId) { throw new Error(`productId is ${productId}`) }

			const activeTags: Array<HTMLButtonElement> = []

			try {
				const tagIds = await getActiveTagIds(productId)
				for (let i = 0; i < TAGS.length; i++) {
					const tag = TAGS[i]
					for (let j = 0; j < tagIds.length; j++) {
						if (tag.id === tagIds[j]) {
							activeTags.push(createTag(tag.value[lang], tag.color))
						}
					}
				}
			}
			catch (error) { console.error(error) }

			row.append(
				t_td({
					class: [
						"rma-table__body__row__cell",
						"text--align-center"]
				}, [String(productData.product_name)]),
				t_td({
					class: [
						"rma-table__body__row__cell",
						"text--align-center"]
				}, [String(productData.product_sn)]),
				t_td({
					class: [
						"rma-table__body__row__cell",
						"text--align-center"]
				}, [String(data[i].created)]),
				t_td({
					class: [
						"rma-table__body__row__cell",
						"text--align-center"]
				}, [String(data[i].updated)]),
				t_td({
					class: [
						"rma-table__body__row__cell",
						"text--align-center"]
				}, [...activeTags]),
			)

			body.append(row)
		}
	}

	return t_div({ class: ["main-table-container", "grid"] }, [
		t_table({ class: "rma-table" }, [
			t_thead({ class: ["rma-table__header", "grid"] }, [
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.case_id[lang]]),
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.legal_entity[lang]]),
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.customer_name[lang]]),
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.product_name[lang]]),
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.product_sn[lang]]),
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.added[lang]]),
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.updated[lang]]),
				t_th({
					class: [
						"rma-table__header__cell",
						"text--align-center"]
				}, [UI_TXT.case_form.tags[lang]]),
			]),
			body])
	])
}
