import { NAVBAR, ROOT } from "./index";
import { c_caseForm } from "./c_case-form";
import { route } from "./u_router";

export async function openCasePage() {
    const params = new URLSearchParams(window.location.search)
    const id = params.get("id")
    if (!id) {
        window.history.replaceState(null, "", "/404")
        route(null, "/404")
        throw new Error("Path is missing an id parameter.")
    }

    try {
        ROOT.replaceChildren(NAVBAR, await c_caseForm(id))
    } catch (error) {
        console.error(error)
        window.history.replaceState(null, "", "/404")
        route(null, "/404")
        return
    }

    document.title = "Service App | RMA form";
}
