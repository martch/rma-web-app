import { UI_TXT, capitalize, lang } from "./u_language"
import { FILTERS, NAVBAR, ROOT } from "./index"
import { mainTable } from "./c_main-table"
import { FilterState, RmaCaseData } from "./interface"
import { u_getFilterState } from "./c_filters"
import { getTableData } from "./sdk/pocketbase/db"

export async function openMainPage() {
    let filterState: FilterState

    try { filterState = u_getFilterState(FILTERS) }
    catch (error) { throw new Error(String(error)) }

    let data: Array<RmaCaseData>

    try { data = await getTableData(filterState) }
    catch (error) { throw new Error(String(error)) }

    try { ROOT.replaceChildren(NAVBAR, FILTERS, await mainTable(data)) }
    catch (error) { throw new Error(String(error)) }

    document.title = "Service App | " + capitalize(UI_TXT.main[lang], "first")
}
