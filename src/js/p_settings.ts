import { get_USER, NAVBAR, ROOT } from "./index";
import { UI_TXT, capitalize, lang } from "./u_language";
import { handleLocation } from "./u_router";
import { t_button, t_div, t_form, t_input, t_label } from "./u_html-tags";
import { updateUserData } from "./sdk/pocketbase/db";

export function openSettingsPage() {
	ROOT.replaceChildren(NAVBAR, buildSettingsPage())
	document.title = "Service App | " + capitalize(UI_TXT.user[lang], "first")
}

function buildSettingsPage() {
	try {
		return t_div({ class: ["grid", "settings-page"] }, [buildUserForm()])
	} catch (error) {
		console.error(error)
		return ""
	}
}

function buildUserForm() {
	const userData = get_USER()
	if (!userData) {
		console.warn(`userData is ${userData}`)
		handleLocation()
		return ""
	}

	const form = t_form({ class: ["grid", "settings-page__form"] }, [
		t_label({
			for: "display_name",
			text: capitalize(`${UI_TXT.username[lang]}:`, "first"),
		}),
		t_input({
			type: "text",
			id: "display_name",
			name: "display_name",
			value: userData.name,
			attr: { "autocomplete": "off" }
		}),
		t_button({
			class: ["btn", "btn--gray", "btn--normal"],
			text: capitalize(`${UI_TXT.save[lang]}`, "first"),
			onclick: () => {
				updateUserData(userData.id, {
					name: form.getElementsByTagName("input")[0].value
				})
			},
		})
	])

	return form
}
