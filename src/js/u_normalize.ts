// idea source: https://stackoverflow.com/a/990922
export function normalize(string: string): string {
        let result = string.toLowerCase();
        result = result.replace(new RegExp("\\s", "g"), "");
        result = result.replace(new RegExp("[āàáâãäå]", "g"), "a");
        result = result.replace(new RegExp("[čç]", "g"), "c");
        result = result.replace(new RegExp("[ēèéêë]", "g"), "e");
        result = result.replace(new RegExp("[ģ]", "g"), "g");
        result = result.replace(new RegExp("[īìíîï]", "g"), "i");
        result = result.replace(new RegExp("[ķ]", "g"), "k");
        result = result.replace(new RegExp("[ļ]", "g"), "l");
        result = result.replace(new RegExp("[ņñ]", "g"), "n");
        result = result.replace(new RegExp("[ōòóôõö]", "g"), "o");
        result = result.replace(new RegExp("[ŗ]", "g"), "r");
        result = result.replace(new RegExp("[š]", "g"), "s");
        result = result.replace(new RegExp("[ūùúûü]", "g"), "u");
        result = result.replace(new RegExp("[ýÿ]", "g"), "y");
        result = result.replace(new RegExp("ž", 'g'), "z");
        result = result.replace(new RegExp("æ", 'g'), "ae");
        result = result.replace(new RegExp("œ", 'g'), "oe");
        result = result.replace(new RegExp("\\W", 'g'), "");
        return result;
}
