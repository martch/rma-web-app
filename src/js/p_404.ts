import { NAVBAR, ROOT } from "./index";
import { t_div } from "./u_html-tags";

export function open_404() {
    ROOT.replaceChildren(NAVBAR, buildContent());
    document.title = "Service App | 404";
}

function buildContent() {
    const content = t_div({ class: "p_404message" }, ["Error p_404 Page not found."])
    return content;
}
