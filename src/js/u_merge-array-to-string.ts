export function u_mergeArrayToString(array: string[]): string {
	if (!Array.isArray(array)) {
		if (!array) { return ""; }
		return array;
	}

	while (array.includes("")) { array.splice(array.indexOf(""), 1); }

	let output = "";

	for (let i = 0; i < array.length; i++) {
		if (i > 0) { output += " // "; }
		output += array[i];
	}

	return output;
}
