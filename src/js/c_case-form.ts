import { UI_TXT, capitalize, lang } from "./u_language";
import {
	t_a,
	t_div,
	t_fieldset,
	t_form,
	t_input,
	t_label,
	t_legend,
	t_option,
	t_select
} from "./u_html-tags";
import { ProductData, RmaCaseData, } from "./interface";
import { APP_DATA } from "./index";
import { getCaseData, getCaseDataCommon, getCaseDataProduct, getRecordUpdated, setCaseData } from "./sdk/pocketbase/db";

export async function c_caseForm(id: string) {
	let caseData: RmaCaseData
	try {
		caseData = await getCaseData(id)
	} catch (error) {
		throw new Error(String(error))
	}

	if (!caseData.expand) {
		throw new Error(`caseData.expand is ${caseData.expand}`)
	} else if (!caseData.expand.product_data) {
		throw new Error(`caseData.expand.product_data is ${caseData.expand.product_data}`)
	}


	const registrationFormLink = t_a({
		class: ["button", "btn--gray", "btn--normal"],
		href: "",
		text: capitalize(UI_TXT.registration_form[lang], "first"),
		useRouter: false,
	})

	registrationFormLink.addEventListener("click", () => { openForm("./forms/registration.html", caseData) })

	const releaseFormLink = t_a({
		class: ["button", "btn--gray", "btn--normal"],
		href: "",
		text: capitalize(UI_TXT.release_form[lang], "first"),
		useRouter: false,
	})

	releaseFormLink.addEventListener("click", () => { openForm("./forms/release.html", caseData) })

	const returnFormLink = t_a({
		class: ["button", "btn--gray", "btn--normal"],
		href: "",
		text: capitalize(UI_TXT.return_form[lang], "first"),
		useRouter: false,
	})

	returnFormLink.addEventListener("click", () => { openForm("./forms/return.html", caseData) })

	const form = t_form({ class: ["add-case-form"] }, [
		t_div({
			class: ["grid", "add-case-form__options"],
		}, [
			t_a({
				class: ["button", "btn--red", "btn--normal"],
				href: "/main",
				text: capitalize(UI_TXT.close[lang], "first"),
				useRouter: true,
			}),
			registrationFormLink,
			releaseFormLink,
			returnFormLink,
		]
		),
	])

	try { listenForChanges(form, caseData) }
	catch (error) { throw new Error(String(error)) }

	form.append(
		formField(
			"text",
			"legal_entity_name",
			UI_TXT.case_form.legal_entity[lang],
			String(caseData.legal_entity_name),
			"rma_cases",
			caseData
		),
		formField(
			"text",
			"customer_name",
			UI_TXT.case_form.customer_name[lang],
			String(caseData.customer_name),
			"rma_cases",
			caseData
		),
		formField(
			"text",
			"customer_phone",
			UI_TXT.case_form.phone[lang],
			String(caseData.customer_phone),
			"rma_cases",
			caseData
		),
		formField(
			"text",
			"customer_email",
			UI_TXT.case_form.email[lang],
			String(caseData.customer_email),
			"rma_cases",
			caseData
		),
		formField(
			"text",
			"customer_order_id",
			UI_TXT.case_form.order[lang],
			String(caseData.customer_order_id),
			"rma_cases",
			caseData
		),
		formField(
			"date",
			"customer_order_date",
			UI_TXT.case_form.order_date[lang],
			(() => {
				if (caseData.customer_order_date) {
					return new Date(String(caseData.customer_order_date))
						.toISOString()
						.substring(0, 10)
				}
				else { return "" }
			})(),
			"rma_cases",
			caseData
		),
		formField(
			"text",
			"customer_bank_account",
			UI_TXT.case_form.bank_account[lang],
			String(caseData.customer_bank_account),
			"rma_cases",
			caseData
		),
	)

	const productCount = caseData.expand.product_data.length

	for (let i = 0; i < productCount; i++) {
		try {
			await addProduct(form, caseData.expand.product_data[i], caseData)
		} catch (error) {
			throw new Error(String(error))
		}
	}

	return form
}

async function addProduct(
	form: HTMLFormElement,
	productData: ProductData,
	caseData: RmaCaseData) {
	const numberProducts = form.getElementsByClassName("add-case-form__product").length
	const productId = numberProducts + 1
	const productIds = caseData.product_data
	if (!productIds) { throw new Error(`productIds is ${productIds}`) }

	const product = t_fieldset({
		id: `add_case_form_product_${productId}`,
		class: "add-case-form__product",
	}, [
		t_legend({}, [
			t_a({
				class: ["button", "btn--short", "btn--gray"],
				text: "log",
				href: `/log?caseId=${caseData.id}&prodId=${productIds[productId - 1]}`,
				useRouter: true,
			}),
			`${capitalize(UI_TXT.product[lang], "first")} ${productId}`
		]),
	])

	const prodId = String(productId)

	product.append(
		formField(
			"text",
			"product_name",
			UI_TXT.case_form.product_name[lang],
			String(productData.product_name),
			"product_data",
			caseData,
			prodId,
		),
		formField(
			"text",
			"product_sn",
			UI_TXT.case_form.product_sn[lang],
			String(productData.product_sn),
			"product_data",
			caseData,
			prodId,
		),
		formField(
			"text",
			"product_price",
			UI_TXT.case_form.product_price[lang],
			String(productData.product_price),
			"product_data",
			caseData,
			prodId,
		),
		formField(
			"text",
			"product_notes",
			UI_TXT.case_form.product_notes[lang],
			String(productData.product_notes),
			"product_data",
			caseData,
			prodId,
		),
		formField(
			"text",
			"product_return_reason",
			UI_TXT.case_form.return_reason[lang],
			String(productData.product_return_reason),
			"product_data",
			caseData,
			prodId,
		),
		formField(
			"select",
			"product_requested_solution",
			UI_TXT.case_form.requested_solution[lang],
			String(productData.product_requested_solution),
			"product_data",
			caseData,
			prodId,
		),
	)

	form.append(product)
}

function formField(
	type: "text" | "select" | "date",
	fieldName: string,
	labelText: string,
	value: string,
	dbTable: "rma_cases" | "product_data",
	caseData: RmaCaseData,
	prodId?: string,
) {
	const fieldData = {
		db_table: dbTable,
		current_value: value,
		timeout_id: 0,
		timeout_delay: 2000,
	}
	const field = new DocumentFragment()

	const fieldId = prodId ? fieldName + "_" + prodId : fieldName
	const label = t_label({
		class: "text--align-right",
		for: prodId ? fieldName + "_" + prodId : fieldName,
		text: labelText,
	})

	field.append(label)

	let input: HTMLInputElement | HTMLSelectElement

	switch (type) {
		case "text":
		case "date":
			input = t_input({
				type: type,
				id: fieldId,
				name: fieldId,
				value: fieldData.current_value,
				attr: {
					"data-searchable": "false"
				},
			})
			break
		case "select":
			input = t_select({
				id: fieldId,
				name: fieldId,
			}, [
				t_option({
					value: "n_a",
					text: UI_TXT.n_a[lang]
				})
			])

			const options = APP_DATA.requestedSolutions
			if (options) {
				for (let i = 0; i < options.length; i++) {
					const name = options[i].name
					const value = options[i].value[lang]
					const option = t_option({
						value: name,
						text: value,
					})
					if (name === fieldData.current_value) {
						option.selected = true
					}
					input.append(option)
				}
			} else {
				console.warn("APP_DATA is missing requested solution options.")
			}
			break
	}

	input.addEventListener("input", () => {
		if (input.value !== fieldData.current_value) { markAsChanged(input) }
		else { markAsNormal(input) }
		saveFieldValue(input, fieldName, fieldData, caseData)
	})

	field.append(input)

	return field
}

function saveFieldValue(
	field: HTMLInputElement | HTMLSelectElement,
	fieldName: string,
	fieldData: {
		current_value: string,
		timeout_id: number,
		timeout_delay: number,
		db_table: "product_data" | "rma_cases",
	},
	caseData: RmaCaseData,
) {
	clearTimeout(fieldData.timeout_id);
	fieldData.timeout_id = window.setTimeout(async () => {
		if (isFieldChanged(field)) {
			const dataToSave: {
				field_name: string
				field_value: string
			} = {
				field_name: fieldName,
				field_value: field.value,
			}

			const targetTable = fieldData.db_table

			if (targetTable === "product_data") {
				const recordIds = caseData.product_data as Array<string>
				const fieldNameLastChar = field.name.substring(field.name.length - 1)
				const index = Number(fieldNameLastChar) - 1

				const expandedData = caseData.expand
				if (!expandedData) {
					return console.error(`expandedData is ${expandedData}`)
				}
				const productData = expandedData.product_data
				if (!productData) {
					return console.error(`productData is ${productData}`)
				}

				try { await setCaseData(targetTable, recordIds[index], dataToSave) }
				catch (error) { return console.error(error) }

			} else {
				const recordId = caseData.id
				if (!recordId) { return console.error(`recordId is ${recordId}`) }

				try { await setCaseData(targetTable, recordId, dataToSave) }
				catch (error) { return console.error(error) }
			}

			console.info("Database record updated successfully.")
			fieldData.current_value = field.value
			markAsNormal(field)
		}
	}, fieldData.timeout_delay)
}

function markAsChanged(el: Element) { el.classList.add("changed-input") }
function markAsNormal(el: Element) { el.classList.remove("changed-input") }
function isFieldChanged(el: Element) { return el.classList.contains("changed-input") }

async function openForm(href: string, data: RmaCaseData) {
	sessionStorage.setItem("case_data", JSON.stringify(data))
	const windowParams =
		'left=0,top=0,width=794,height=1123,toolbar=0,scrollbars=0,status=0';
	window.open(href, "_blank", windowParams);
}

function listenForChanges(form: HTMLFormElement, caseData: RmaCaseData) {
	const caseId = caseData.id
	if (!caseId) { throw new Error(`caseData.id is ${caseId}`) }

	const productIds = caseData.product_data
	if (!productIds) { throw new Error(`caseData.product_data is ${productIds}`) }
	productIds.forEach((item) => {
		if (typeof item !== "string") {
			throw new Error("caseData.product_data is not Array<string>.")
		}
	})

	const expandField = caseData.expand
	if (!expandField) { throw new Error(`caseData.expand is ${expandField}`) }
	const productData = expandField.product_data
	if (!productData) { throw new Error(`caseData.product_data is ${productData}`) }
	productData.forEach((item) => {
		if (typeof item !== "object") {
			throw new Error("caseData.product_data is not Array<object>.")
		}
	})

	const intervalId = setInterval(async () => {
		const oldCommonDataUpdatedDate = caseData.updated
		if (!oldCommonDataUpdatedDate) {
			return console.error(`caseData.updated is ${caseData.updated}`)
		}

		let newCommonDataUpdatedDate: string

		try { newCommonDataUpdatedDate = await getRecordUpdated("rma_cases", caseId) }
		catch (error) { return console.error(error) }

		if (oldCommonDataUpdatedDate !== newCommonDataUpdatedDate) {
			console.info("Case data has been updated since loading the form.")

			let newCaseDataCommon: RmaCaseData

			try { newCaseDataCommon = await getCaseDataCommon(caseId) }
			catch (error) { return console.error(error) }

			Object.keys(newCaseDataCommon).forEach((key) => {
				if (caseData[key] !== newCaseDataCommon[key]) {
					const element = form.elements.namedItem(key)
					if (element) {
						const field = element as
							HTMLInputElement
							| HTMLSelectElement
							| HTMLTextAreaElement
						field.blur()
						field.value = String(newCaseDataCommon[key])
					}
				}
			})
			caseData.updated = newCaseDataCommon.updated
		}

		for (let i = 0; i < productIds.length; i++) {
			const productId = productIds[i] as string
			const oldProductDataUpdatedDate = productData[i].updated
			if (!oldProductDataUpdatedDate) {
				return console.error(`productData[${i}].updated is ${productData[i].updated}`)
			}

			let newProductDataUpdatedDate: string

			try { newProductDataUpdatedDate = await getRecordUpdated("product_data", productId) }
			catch (error) { return console.error(error) }

			if (oldProductDataUpdatedDate !== newProductDataUpdatedDate) {
				console.info("Product data has been updated since loading the form.")

				let newCaseDataProduct: ProductData

				try { newCaseDataProduct = await getCaseDataProduct(productId) }
				catch (error) { return console.error(error) }

				Object.keys(newCaseDataProduct).forEach((key) => {
					if (productData[i][key] !== newCaseDataProduct[key]) {
						const element = form.elements.namedItem(`${key}_${i + 1}`)
						if (element) {
							const field = element as
								HTMLInputElement
								| HTMLSelectElement
								| HTMLTextAreaElement
							field.blur()
							field.value = String(newCaseDataProduct[key])
						}
					}
				})
				productData[i].updated = newCaseDataProduct.updated
			}
		}

	}, 1000)

	window.addEventListener("userchangedlocation", () => { clearInterval(intervalId) })
}
