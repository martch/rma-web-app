import { APP_DATA, get_USER, NAVBAR, ROOT } from "./index"
import { UI_TXT, capitalize, getUserLang } from "./u_language"
import { t_button, t_div, t_fieldset, t_form, t_input, t_label, t_legend, t_option, t_select } from "./u_html-tags"
import { ProductData, RmaCaseData } from "./interface"
import { route } from "./u_router"
import { addNewCase, getNewCaseId } from "./sdk/pocketbase/db"

const lang = getUserLang()

export async function openNewCasePage() {
    ROOT.replaceChildren(NAVBAR, c_form())
    document.title = "Service App | " + capitalize(UI_TXT.new_case[lang], "first")
}

async function getFormData(form: HTMLFormElement) {
    let newCaseId: number

    try { newCaseId = await getNewCaseId() }
    catch (error) { throw new Error(`Failed to get newCaseID. Error: ${error}`) }

    const userData = get_USER()
    if (!userData) { throw new Error(`userData is ${userData}`) }

    const productData: Array<ProductData> = []

    const products = form.getElementsByClassName("add-case-form__product")
    for (let i = 0; i < products.length; i++) {
        const id = products[i].id.split("_").pop()
        const data: ProductData = {
            product_name: form[`product_name_${id}`].value,
            product_sn: form[`product_sn_${id}`].value,
            product_price: form[`product_price_${id}`].value,
            product_notes: form[`product_notes_${id}`].value,
            product_return_reason: form[`product_return_reason_${id}`].value,
            product_requested_solution: form[`product_requested_solution_${id}`].value,
        }
        productData.push(data)
    }

    // TODO: validate form inputs (phone has to be number, email has to be email)
    const data: RmaCaseData = {
        case_id: newCaseId,
        created_by: userData.id,
        customer_bank_account: form.customer_bank_account.value,
        customer_email: form.customer_email.value,
        customer_name: form.customer_name.value,
        customer_phone: form.customer_phone.value,
        legal_entity_name: form.legal_entity_name.value,
        customer_order_id: form.customer_order_id.value,
        customer_order_date: form.customer_order_date.value,
        product_data: productData,
    }

    return data
}

function c_form() {
    const form = t_form({ class: ["add-case-form"] }, [
        t_div({
            class: ["grid", "add-case-form__options"],
        }, [
            t_button({
                class: ["btn", "btn--red", "btn--normal"],
                text: capitalize(UI_TXT.reset[lang], "first"),
                onclick: () => { form.reset() },
            }),
            t_button({
                class: ["btn", "btn--gray", "btn--normal"],
                text: capitalize(UI_TXT.add_product[lang], "first"),
                onclick: async () => { await c_addProduct(form) },
            }),
            t_button({
                class: ["btn", "btn--green", "btn--normal"],
                text: capitalize(UI_TXT.add[lang], "first"),
                onclick: async () => {
                    try {
                        const caseId = await addNewCase(await getFormData(form))
                        console.info("New case added succesfully.")
                        route(null, `/case?id=${caseId}`)
                    }
                    catch (error) { console.error(error) }
                },
            }),
        ]
        ),
    ])

    form.append(
        formField(
            "text",
            "legal_entity_name",
            UI_TXT.case_form.legal_entity[lang],
        ),
        formField(
            "text",
            "customer_name",
            UI_TXT.case_form.customer_name[lang],
        ),
        formField(
            "text",
            "customer_phone",
            UI_TXT.case_form.phone[lang],
        ),
        formField(
            "text",
            "customer_email",
            UI_TXT.case_form.email[lang],
        ),
        formField(
            "text",
            "customer_order_id",
            UI_TXT.case_form.order[lang],
        ),
        formField(
            "date",
            "customer_order_date",
            UI_TXT.case_form.order_date[lang],
        ),
        formField(
            "text",
            "customer_bank_account",
            UI_TXT.case_form.bank_account[lang],
        ),
    )

    return form
}

async function c_addProduct(form: HTMLFormElement) {
    const numberProducts = form.getElementsByClassName("add-case-form__product").length
    if (numberProducts > 9) {
        console.info("Reached max number of products")
        return
    }

    let i = 0
    const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
    let result = ""
    do {
        const index = Math.floor(Math.random() * (chars.length - 1))
        result += chars.substring(index, index + 1)
        i++
    } while (i < 5)
    const productId = result

    const product = t_fieldset({
        id: `add_case_form_product_${productId}`,
        class: "add-case-form__product",
    }, [
        t_legend({}, [
            `${capitalize(UI_TXT.product[lang], "first")} ${productId}`
        ]),
    ])

    product.append(
        formField(
            "text",
            `product_name_${productId}`,
            UI_TXT.case_form.product_name[lang],
        ),
        formField(
            "text",
            `product_sn_${productId}`,
            UI_TXT.case_form.product_sn[lang],
        ),
        formField(
            "text",
            `product_price_${productId}`,
            UI_TXT.case_form.product_price[lang],
        ),
        formField(
            "text",
            `product_notes_${productId}`,
            UI_TXT.case_form.product_notes[lang],
        ),
        formField(
            "text",
            `product_return_reason_${productId}`,
            UI_TXT.case_form.return_reason[lang],
        ),
        formField(
            "select",
            `product_requested_solution_${productId}`,
            UI_TXT.case_form.requested_solution[lang],
        ),
        t_button({
            class: ["btn", "btn--red", "btn--normal"],
            text: capitalize(UI_TXT.remove_product[lang], "first"),
            onclick: (el: Event) => {
                (el.target as HTMLElement).parentElement?.remove()
            }
        }),
    )

    form.append(product)
    form.scrollTo({ top: form.scrollHeight })
}

function formField(
    type: "text" | "select" | "date" | "checkbox",
    fieldName: string,
    labelText: string,
    attr?: { [attr: string]: string },
    action?: Function,
) {
    const field = new DocumentFragment()
    const label = t_label({
        class: "text--align-right",
        for: fieldName,
        text: labelText,
    })

    field.append(label)

    let input: HTMLInputElement | HTMLSelectElement

    switch (type) {
        case "text":
        case "date":
        case "checkbox":
            input = t_input({
                type: type,
                id: fieldName,
                name: fieldName,
                attr: {
                    "data-searchable": "false"
                },
            })
            break
        case "select":
            input = t_select({
                id: fieldName,
                name: fieldName,
                value: "n_a"
            }, [
                t_option({
                    value: "n_a",
                    text: UI_TXT.n_a[lang]
                })
            ])

            const options = APP_DATA.requestedSolutions
            if (options) {
                for (let i = 0; i < options.length; i++) {
                    input.append(t_option({
                        value: options[i].name,
                        text: options[i].value[lang],
                    }));
                }
            } else {
                console.warn("APP_DATA is missing requested solution options.")
            }
            break
    }

    if (attr) {
        Object.keys(attr).forEach((key) => {
            input.setAttribute(key, attr[key])
        })
    }

    if (action) {
        input.addEventListener("input", () => { action() })
    }

    field.append(input)

    return field
}
