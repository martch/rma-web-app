import { openNewCasePage } from "./p_add"
import { openCasePage } from "./p_case"
import { buildLoginPage } from "./p_login"
import { openMainPage } from "./p_main"
import { openSettingsPage } from "./p_settings"
import { open_404 } from "./p_404"
import { p_log } from "./p_log"
import { get_USER, set_USER } from "./index"
import { isAuthValid } from "./sdk/pocketbase/auth"

export function route(event: Event | null, path: string) {
    if (event) { event.preventDefault() }
    window.history.pushState({}, "", path)
    handleLocation()
}

const navEvent = new CustomEvent("userchangedlocation", { detail: "User has changed loaction." })

const routes: { [s: string]: CallableFunction } = {
    "/404": () => { open_404() },
    "/main": async () => { await openMainPage() },
    "/": async () => { route(null, "/main") },
    "/login": async () => {
        if (!get_USER()) {
            window.history.replaceState(null, "", "/login")
            buildLoginPage()
            return
        }
        window.location.pathname = "/main"
        window.history.replaceState(null, "", "/main")
    },
    "/new": async () => {
        try { await openNewCasePage() }
        catch (error) { console.error(error) }
    },
    "/settings": async () => { openSettingsPage() },
    "/case": async () => {
        try { await openCasePage() }
        catch (error) { console.error(error) }
    },
    "/log": async () => { await p_log() },
}

export function handleLocation() {
    if (!isAuthValid()) { set_USER(null) }

    const path = window.location.pathname

    get_USER()
        ? routes[path]
            ? routes[path]()
            : routes["/404"]()
        : routes["/login"]()

    window.dispatchEvent(navEvent)
}

window.addEventListener('popstate', () => { handleLocation() })
