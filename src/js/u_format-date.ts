export class FormatDate {
        constructor(timestamp: number) {
                const date = new Date(Number(timestamp));
                const year = String(date.getFullYear());
                let month = date.getMonth() + 1;
                let monthFormatted: string;
                let day = date.getDate();
                let dayFormatted: string;

                if (month < 10) { monthFormatted = `0${month}`; }
                else { monthFormatted = String(month); }

                if (day < 10) { dayFormatted = `0${day}`; }
                else { dayFormatted = String(day); }

                return `${year}-${monthFormatted}-${dayFormatted}`;
        }
}
